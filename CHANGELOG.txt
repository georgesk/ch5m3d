CH5M3D 1.2.2, 2013-06-27
------------------------
- Added ability to include more than one gallery on a single web page.
- Fixed bug with deleting attached hydrogen atoms.
- Removed "view-only" javascript library.  All files in variations folder now call
  main library.  (It was not documented which functions were missing from this
  library, and the size reduction was minimal).


CH5M3D 1.2.1, 2013-06-24
------------------------
- Added a number of web pages to the variations folder to provide more examples
  illustrating ways this library can be used to create web pages.  Most notable
  addition was a gallery page showing multiple floating boxes and a sample script
  that creates mirror images of 2-butanol.
- Updated sections of the documentation to better describe some user-accessible
  functions.


CH5M3D 1.2, 2013-06-10
-----------------------
- Fixed a number of bugs.  Most notable changes in routine to calculate charges.


CH5M3D 1.1, 2013-04-10
----------------------
- Development version. Never released.


CH5M3D 1.0, 2013-03-13
----------------------
- Initial release

