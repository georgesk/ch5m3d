<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">

<link rel="stylesheet" type="text/css" href="ch5m3dq.css" />
<script type="text/javascript" src="ch5m3dq.js"></script>
<title>CH5M3D</title>
</head>

<body  onload="initialize()">


<p class="QCHeader">
  CH<sub>5</sub>M<sub>3D</sub>
  </p>
<ul id="Toolbar">

  <li><a href="../index.html">CH5M3D Home</a><ul>
    </ul></li>
<li><a href="qchem.html">CH5M3D - GAMESS</a><ul>
    </ul></li>
  <li>Documentation<ul>
    <li><a href="../doc/introduction.html">Introduction</a></li>
    <li><a href="../doc/installation.html">Installation</a></li>
    <li><a href="../doc/browser.html">Web Browsers</a></li>
    <li><a href="../doc/view.html">User Interface</a></li>
    <li><a href="../doc/keyboard.html">Keyboard/Mouse</a></li>
    <li><a href="../doc/drawing.html">Drawing</a></li>
    <li><a href="../doc/fileformat.html">File Format</a></li>
    <li><a href="../documentation.pdf">PDF Manual</a></li>
    </ul></li>
  <li>Variations<ul>
    <li><a href="../doc/description.html">Description</a></li>
    <li><a href="../variations/serverView.html">Pre-Load</a></li>
    <li><a href="../variations/chooser.html">Chooser</a></li>
    <li><a href="../variations/gallery.html">Gallery</a></li>
    <li><a href="../variations/viewer300.html">Viewer (only)</a></li>
    <li><a href="../variations/viewer2x.html">View 2 Windows</a></li>
    <li><a href="../variations/two.html">Two Windows</a></li>
    <li><a href="../variations/api.html">Javascript</a></li>
    <li><a href="qchem.php">Quantum Interface</a></li>
    </ul></li>
  <li>Information<ul>
    <li><a href="../doc/about.html">About</a></li>
    <li><a href="http://sourceforge.net/projects/ch5m3d/">Project Homepage</a></li>
    <li><a href="../doc/apidoc.html">Library API Info</a></li>
    <li><a href="https://www.gnu.org/licenses/gpl.html">GNU License</a></li>
    </ul></li>
  <li class="UserName">Welcome <? echo $_SERVER['PHP_AUTH_USER'] ?></li>
  </ul>



<p class="indent">Complete the following section to change your password.</p>
<form name="QCPassForm">
  <label for="username">Username:</label>
    <input type="text" id="username" maxlength="15" /><br />
  <label for="oldPass">Old Password:</label>
    <input type="password" id="oldPass" maxlength="20" /><br />
  <label for="newPass">New Password:</label>
    <input type="password" id="newPass" maxlength="20" /><br />
  <label for="verify">Verify New Password:</label>
    <input type="password" id="verify" maxlength="20" /><br />
  <label for="changePW">&nbsp;</label>
    <input type="button" id="changePW" onclick="changePass()" value="Change Password" />

<p class="indent">Return to <a href="index.php">Quantum Chemistry Interface</a> page.</p>

<script type="text/javascript">
// -------------------- Functions to process password requests --------------------

//
//  Function to change user's password
//
function changePass() {

  var url = "newpass.php";
  var QCUser    = document.QCPassForm.username.value;
  var QColdpass = document.QCPassForm.oldPass.value;
  var QCnewpass = document.QCPassForm.newPass.value;
  var QCverify  = document.QCPassForm.verify.value;

  if (QCnewpass != QCverify) {
    alert("New passwords do NOT match");
    return;
    }

  // Create XML object
  // Should(?) work for IE7+, Firefox, Chrome, Opera, Safari
  try {
    var xmlhttp = new XMLHttpRequest();
    } catch(e) {
      alert("ERROR: Unable to establish connection to change password.");
      return;
      }

  // Respond to php function output
  xmlhttp.onreadystatechange = function() {
    if(xmlhttp.readyState == 4  &&  xmlhttp.status==200) {
      var status = xmlhttp.responseText;
      // alert("RESPONSE: ["+status+"]");
      if (status == 0) {
        window.location = "index.php";
        }
      if (status != 0) {
        alert("ERROR: "+status);
        }
      }
    }

  // Combine information to send to PHP file
  contents  = "username=" + QCUser;
  contents += "&oldpass=" + QColdpass;
  contents += "&newpass=" + QCnewpass;

  // "Post" data to php routine
  xmlhttp.open("POST",url,true);
  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  xmlhttp.send(contents);
  }

</script>

</body>
</html>

<div class="Footer" id="Footer">
  The chem3d.js library copyright © 2013 by Clarke Earley<br />
  and is distributed under the terms of the
  <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License</a>.
  </div>

</body>
</html>
